import {
  Controller,
  Get,
  Inject,
  Param,
  ParseArrayPipe,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { keyMedicallicensesService } from './lib/constants';
import { IMedicallicensesService } from './services/medicallicenses.service.interface';
import { ApiOkResponsePaginated } from './lib/generics.swagger';
import { MedicalLicenseDTO } from './dto/medicallicenses.dto';

@Controller('medicallicenses')
export class MedicallicensesController {
  constructor(
    @Inject(keyMedicallicensesService)
    private readonly medicallicensesService: IMedicallicensesService,
  ) {}

  @Get('affiliate/:id/licenses')
  @ApiOkResponsePaginated(MedicalLicenseDTO)
  async getLicensesByAffiliateId(
    @Param('id', ParseIntPipe) id: number,
    @Query('page') page: number,
    @Query('limit') limit: number,
  ) {
    return await this.medicallicensesService.getLicensesByAffiliate(
      id,
      page,
      limit,
    );
  }

  @Get('affiliates')
  @ApiOkResponsePaginated(MedicalLicenseDTO)
  async getLicensesByAffiliates(
    @Query('rut', ParseArrayPipe) affiliateRuts: number[],
  ) {
    return await this.medicallicensesService.getLicensesByAffiliates(
      affiliateRuts,
    );
  }
}
