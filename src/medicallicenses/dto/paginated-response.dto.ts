import { ApiProperty } from '@nestjs/swagger';

export class PaginatedResponseDTO<T> {
  @ApiProperty()
  total: number;
  @ApiProperty()
  offset: number;
  @ApiProperty()
  limit: number;
  data: T;
}
