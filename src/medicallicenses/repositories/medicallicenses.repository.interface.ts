import { MedicalLicenseDTO } from '../dto/medicallicenses.dto';
import { PaginatedResponseDTO } from '../dto/paginated-response.dto';

export interface IMedicallicensesRepository {
  getLicensesByAffiliate(
    affiliateId: number,
    page: number,
    limit: number,
  ): Promise<PaginatedResponseDTO<MedicalLicenseDTO[]>>;
  getLicensesByAffiliates(
    affiliateRuts: number[],
  ): Promise<MedicalLicenseDTO[]>;
}
