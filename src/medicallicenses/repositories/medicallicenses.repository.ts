import axios, { AxiosError } from 'axios';
import { Injectable } from '@nestjs/common';
import { IMedicallicensesRepository } from './medicallicenses.repository.interface';
import { MedicalLicenseDTO } from '../dto/medicallicenses.dto';
import { ConfigService } from '@nestjs/config';
import { PaginatedResponseDTO } from '../dto/paginated-response.dto';

@Injectable()
export class MedicallicensesRepository implements IMedicallicensesRepository {
  constructor(private configService: ConfigService) {}

  private URL_BASE = this.configService.get<string>('URL_BASE');

  public async getLicensesByAffiliate(
    affiliateId: number,
    page: number,
    limit: number,
  ) {
    try {
      const licenses = await axios.get<
        PaginatedResponseDTO<MedicalLicenseDTO[]>
      >(
        `${this.URL_BASE}/api/MedicalLicenses/affiliate/${affiliateId}?page=${page}&limit=${limit}`,
      );

      return licenses.data;
    } catch (error) {
      const err = error as AxiosError;
      console.error('error al obtener licencias por afiliado', err.message);
      throw err;
    }
  }

  public async getLicensesByAffiliates(affiliateRuts: number[]) {
    try {
      const queryString = affiliateRuts.map((key) => 'rut=' + key).join('&');
      const path = `${this.URL_BASE}/api/MedicalLicenses/affiliates?${queryString}`;

      const licenses = await axios.get<MedicalLicenseDTO[]>(path);

      return licenses.data;
    } catch (error) {
      const err = error as AxiosError;
      console.error('error al obtener licencias por afiliado', err.message);
      throw err;
    }
  }
}
