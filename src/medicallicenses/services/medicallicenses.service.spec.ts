import { Test } from '@nestjs/testing';
import { MedicallicensesService } from './medicallicenses.service';
import { keyMedicallicensesRepository } from '../lib/constants';
import { IMedicallicensesService } from './medicallicenses.service.interface';

describe('MedicallicensesService', () => {
  let service: IMedicallicensesService;

  afterEach(() => {
    jest.clearAllMocks();
  });

  const mockMedicalLicensesRepo = {
    getLicensesByAffiliate: jest
      .fn()
      .mockImplementation((affiliateId: number, page: number, limit: number) =>
        Promise.resolve({
          limit,
          offset: page - 1,
          total: 1,
          data: [
            {
              medicalLicenseNumber: 1,
              startDate: new Date(),
              endDate: new Date(),
              status: 'Pendiente',
            },
          ],
        }),
      ),
  };

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [],
      providers: [
        {
          provide: keyMedicallicensesRepository,
          useValue: mockMedicalLicensesRepo,
        },
        MedicallicensesService,
      ],
    }).compile();

    service = await moduleRef.get<IMedicallicensesService>(
      MedicallicensesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('se obtiene varias licencias por afiliado', async () => {
    const affiliateId = 1;
    const page = 1;
    const limit = 10;

    const medicalLicensesByAffilite = await service.getLicensesByAffiliate(
      affiliateId,
      page,
      limit,
    );

    expect(mockMedicalLicensesRepo.getLicensesByAffiliate).toBeCalledTimes(1);
    expect(mockMedicalLicensesRepo.getLicensesByAffiliate).toBeCalledWith(
      affiliateId,
      page,
      limit,
    );
    expect(medicalLicensesByAffilite.data).toHaveLength(1);
  });

  it('no hay licencias por afiliado', async () => {
    const mockMedicalLicensesRepoReturnsEmpty = {
      getLicensesByAffiliate: jest
        .fn()
        .mockImplementation(
          (affiliateId: number, page: number, limit: number) =>
            Promise.resolve({
              limit,
              offset: page - 1,
              total: 1,
              data: [],
            }),
        ),
    };

    const moduleRef = await Test.createTestingModule({
      controllers: [],
      providers: [
        {
          provide: keyMedicallicensesRepository,
          useValue: mockMedicalLicensesRepoReturnsEmpty,
        },
        MedicallicensesService,
      ],
    }).compile();

    service = await moduleRef.get<IMedicallicensesService>(
      MedicallicensesService,
    );

    const affiliateId = 1;
    const page = 1;
    const limit = 10;

    const medicalLicensesByAffilite = await service.getLicensesByAffiliate(
      affiliateId,
      page,
      limit,
    );

    expect(
      mockMedicalLicensesRepoReturnsEmpty.getLicensesByAffiliate,
    ).toBeCalledTimes(1);
    expect(
      mockMedicalLicensesRepoReturnsEmpty.getLicensesByAffiliate,
    ).toBeCalledWith(affiliateId, page, limit);
    expect(medicalLicensesByAffilite.data).toHaveLength(0);
  });
});
