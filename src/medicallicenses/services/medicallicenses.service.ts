import { Inject, Injectable } from '@nestjs/common';
import { IMedicallicensesService } from './medicallicenses.service.interface';
import { keyMedicallicensesRepository } from '../lib/constants';
import { IMedicallicensesRepository } from '../repositories/medicallicenses.repository.interface';

@Injectable()
export class MedicallicensesService implements IMedicallicensesService {
  constructor(
    @Inject(keyMedicallicensesRepository)
    private readonly medicallicensesRepository: IMedicallicensesRepository,
  ) {}

  public async getLicensesByAffiliate(
    affiliateId: number,
    page: number,
    limit: number,
  ) {
    return await this.medicallicensesRepository.getLicensesByAffiliate(
      affiliateId,
      page,
      limit,
    );
  }

  public async getLicensesByAffiliates(affiliateRuts: number[]) {
    return await this.medicallicensesRepository.getLicensesByAffiliates(
      affiliateRuts,
    );
  }
}
