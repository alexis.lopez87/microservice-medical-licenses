import { Module } from '@nestjs/common';
import { MedicallicensesController } from './medicallicenses.controller';
import {
  keyMedicallicensesRepository,
  keyMedicallicensesService,
} from './lib/constants';
import { MedicallicensesService } from './services/medicallicenses.service';
import { MedicallicensesRepository } from './repositories/medicallicenses.repository';

@Module({
  controllers: [MedicallicensesController],
  providers: [
    {
      provide: keyMedicallicensesService,
      useClass: MedicallicensesService,
    },
    {
      provide: keyMedicallicensesRepository,
      useClass: MedicallicensesRepository,
    },
  ],
})
export class MedicallicensesModule {}
