import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MedicallicensesModule } from './medicallicenses/medicallicenses.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [MedicallicensesModule, ConfigModule.forRoot({ isGlobal: true })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
